package com.ibm.bootcamp.librarysystem.dao;

import java.io.NotSerializableException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;


import com.ibm.bootcamp.librarysystem.model.Book;
import com.ibm.bootcamp.librarysystem.model.BookLend;


public class BookDAO{
	private static final String url = "jdbc:mariadb://localhost:3306/projectbootcamp";
	private static final String dbuser = "root";
	private static final String dbpass = "root";
	private static String searchString, dateReturned;
	private static Integer id, bookLendId;

	public static Integer getBookLendId() {
		return bookLendId;
	}

	public static void setBookLendId(Integer bookLendId) {
		BookDAO.bookLendId = bookLendId;
	}

	public static String getDateReturned() {
		return dateReturned;
	}

	public static void setDateReturned(String dateReturned) {
		BookDAO.dateReturned = dateReturned;
	}

	public static Integer getId() {
		return id;
	}

	public static void setId(Integer id) {
		BookDAO.id = id;
	}

	public static String getSearchString() {
		return searchString;
	}

	public static void setSearchString(String searchString) {
		BookDAO.searchString = searchString;
	}
	
	

	public BookDAO(){
		
	}
	
	public void addBook(Book book) {
	Connection conn = null;
	PreparedStatement ps = null;
	try {
		Class.forName("org.mariadb.jdbc.Driver");
	}catch(ClassNotFoundException cnfe){
		cnfe.printStackTrace();
	}
	
	try {
		conn = DriverManager.getConnection(url,dbuser,dbpass);
		ps = conn.prepareStatement("INSERT INTO book (bookId,Title,Author, ISBN,Subject,Location,datePublished,dateAcquired,Available)values (?,?,?,?,?,?,(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),?)");
		ps.setInt(1, book.getBookId());
		ps.setString(2, book.getBookTitle());
		ps.setString(3, book.getBookAuthor());
		ps.setString(4, book.getBookIsbn());
		ps.setString(5, book.getBookSubject());
		ps.setString(6, book.getBookLocation());
		ps.setString(7, book.getBookDatePublished());
		ps.setString(8, book.getBookDateAcquired());
		ps.setString(9, book.getBookStatus());
		ps.execute();
		
//		System.out.println("Book was added Successfully.");
		
	}catch(SQLException sqle) {
		sqle.printStackTrace();
	}finally {
		try{
			conn.close();
			ps.close();
			
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	
	
}
	public static ArrayList<Book> getBook(){
		ArrayList<Book> bookList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Book");
			rs = ps.executeQuery();
			while(rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("bookId"));
				book.setBookTitle(rs.getString("Title"));
				book.setBookAuthor(rs.getString("Author"));
				book.setBookIsbn(rs.getString("ISBN"));
				book.setBookSubject(rs.getString("Subject"));
				book.setBookLocation(rs.getString("Location"));
				book.setBookDatePublished(rs.getString("datePublished"));
				book.setBookDateAcquired(rs.getString("dateAcquired"));
				book.setBookStatus(rs.getString("Available"));
				bookList.add(book);
			}
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return bookList;
	}
	
	public static ArrayList<Book> getResult(){
		String query = getSearchString();
		ArrayList<Book> bookList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try{
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Book Where Title Like '%"+query+"%' or Author like'%"+query+"%' or Subject like '%"+query+"%'or ISBN like '"+query+"'");
			rs = ps.executeQuery();
			while(rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("bookId"));
				book.setBookTitle(rs.getString("Title"));
				book.setBookAuthor(rs.getString("Author"));
				book.setBookIsbn(rs.getString("ISBN"));
				book.setBookSubject(rs.getString("Subject"));
				book.setBookLocation(rs.getString("Location"));
				book.setBookDatePublished(rs.getString("datePublished"));
				book.setBookDateAcquired(rs.getString("dateAcquired"));
				book.setBookStatus(rs.getString("Available"));
				bookList.add(book);
				
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return bookList;
	}
	
	public static ArrayList<Book> displayBookDetails() throws NotSerializableException{
		String query = getSearchString();
		ArrayList<Book> bookList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try{
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Book Where bookId like '"+query+"'");
			rs = ps.executeQuery();
			while(rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("bookId"));
				book.setBookTitle(rs.getString("Title"));
				book.setBookAuthor(rs.getString("Author"));
				book.setBookIsbn(rs.getString("ISBN"));
				book.setBookSubject(rs.getString("Subject"));
				book.setBookLocation(rs.getString("Location"));
				book.setBookDatePublished(rs.getString("datePublished"));
				book.setBookDateAcquired(rs.getString("dateAcquired"));
				book.setBookStatus(rs.getString("Available"));
				bookList.add(book);
				
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return bookList;
	}
	
	public void updateBook(Book book) {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer id = getId();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("Update book SET Location=?, Available=? WHERE bookId="+id+";");
//			ps = conn.prepareStatement("INSERT INTO book (bookId,Title,Author, ISBN,Subject,Location,datePublished,dateAcquired,Available)values (?,?,?,?,?,?,(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),?)");
			ps.setString(1, book.getBookLocation());
			ps.setString(2, book.getBookStatus());
			ps.execute();
			
//			System.out.println("Book was updated Successfully.");
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try{
				conn.close();
				ps.close();
			}catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}

	}
	public void deleteBook() {
		Connection conn = null;
		int id = getId();
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("DELETE From Book where bookId="+id+";");
			ps.execute();
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try{
				conn.close();
				ps.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
	}

	public static ArrayList<Book> getAvailableBook(){
		ArrayList<Book> bookList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Book Where Available='Yes';");
			rs = ps.executeQuery();
			while(rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("bookId"));
				book.setBookTitle(rs.getString("Title"));
				book.setBookAuthor(rs.getString("Author"));
				book.setBookIsbn(rs.getString("ISBN"));
				book.setBookSubject(rs.getString("Subject"));
				book.setBookLocation(rs.getString("Location"));
				book.setBookDatePublished(rs.getString("datePublished"));
				book.setBookDateAcquired(rs.getString("dateAcquired"));
				book.setBookStatus(rs.getString("Available"));
				bookList.add(book);
			}
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return bookList;
	}
	
	public void lendBook(BookLend bookLend) throws SQLIntegrityConstraintViolationException, NotSerializableException{
		
		Connection conn = null;
		PreparedStatement ps = null;
//		Integer id = getId();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("INSERT INTO lend (BookLendId, bookId, BorrowerName, DateBorrowed, DateDue, DateReturned, Returned) values (?,?,?,(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),?)");
			ps.setInt(1, bookLend.getBookLendId());
			ps.setInt(2, bookLend.getBookId());
			ps.setString(3, bookLend.getBorrowerName());
			ps.setString(4, bookLend.getBookBorrowedDate());
			ps.setString(5, bookLend.getBookDueDate());
			ps.setString(6, bookLend.getBookDateReturned());
			ps.setString(7, "No");
			ps.execute();
			
//			System.out.println("Book was updated Successfully.");
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}
		
		finally {
			try{
				conn.close();
				ps.close();
			}catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}
	}
	
	public void lendBookProxy(BookLend bookLend){
		
		Connection conn = null;
		PreparedStatement ps = null;
//		Integer id = getId();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("INSERT INTO lendhistory (BookLendId, bookId, BorrowerName, DateBorrowed, DateDue, DateReturned,Returned) values (?,?,?,(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),(STR_TO_DATE(?, '%Y-%m-%d')),?)");
			ps.setInt(1, bookLend.getBookLendId());
			ps.setInt(2, bookLend.getBookId());
			ps.setString(3, bookLend.getBorrowerName());
			ps.setString(4, bookLend.getBookBorrowedDate());
			ps.setString(5, bookLend.getBookDueDate());
			ps.setString(6, bookLend.getBookDateReturned());
			ps.setString(7, "No");
			ps.execute();
			
//			System.out.println("Book was updated Successfully.");
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try{
				conn.close();
				ps.close();
			}catch(SQLException sqle){
				sqle.printStackTrace();
			}
		}
	}
	
	public void updateStatus() {
		Integer id = getId();
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("Update book SET Available=? WHERE bookId="+id+"");
			ps.setString(1, "No");
			ps.execute();
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
	}
	
	public static ArrayList<Book> dueBook() throws NotSerializableException{
		ArrayList<Book> bookList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Book Where Available='No'");
			rs = ps.executeQuery();
			while(rs.next()) {
				Book book = new Book();
				book.setBookId(rs.getInt("bookId"));
				book.setBookTitle(rs.getString("Title"));
				book.setBookAuthor(rs.getString("Author"));
				book.setBookIsbn(rs.getString("ISBN"));
				book.setBookSubject(rs.getString("Subject"));
				book.setBookLocation(rs.getString("Location"));
				book.setBookDatePublished(rs.getString("datePublished"));
				book.setBookDateAcquired(rs.getString("dateAcquired"));
				book.setBookStatus(rs.getString("Available"));
				bookList.add(book);
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return bookList;
	}
	
	
	public static ArrayList<BookLend> getLendBooks() {
		ArrayList<BookLend> lendList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * from lend WHERE Returned='No'");
			rs = ps.executeQuery();
			while(rs.next()) {
				BookLend bookLend = new BookLend();
				bookLend.setBookLendId(rs.getInt("BookLendId"));
				bookLend.setBookId(rs.getInt("bookId"));
				bookLend.setBorrowerName(rs.getString("BorrowerName"));
				bookLend.setBookBorrowedDate(rs.getString("DateBorrowed"));
				bookLend.setBookDueDate(rs.getString("DateDue"));
//				bookLend.setBookDateReturned(rs.getString("DateReturned"));
				lendList.add(bookLend);
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		return lendList;
	}

	
	public void updateReturn() {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer bookId = getBookLendId();
		String bookReturnId = bookId.toString();
		String returneddate = getDateReturned();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		//SetReturnedDate
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("Update lend SET DateReturned=(STR_TO_DATE(?, '%Y-%m-%d')) WHERE BookLendId="+bookReturnId+"");
			ps.setString(1, returneddate);
			ps.execute();
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
	}
	
	public Integer returnBookID() {
		Integer bookLendId = getBookLendId();
		Integer bookReturnId = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection conn = null;
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT bookId from lend WHERE BookLendId="+bookLendId+"");
			rs = ps.executeQuery();
			while (rs.next()) {
				bookReturnId = rs.getInt("bookId");
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}
	
		return bookReturnId;
		
	}
	
	public void updateStatusReturn() {
		Integer bookId= getId();
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("Update book SET Available=? WHERE bookId="+bookId+"");
			ps.setString(1, "Yes");
			ps.execute();
			
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
				
			}
		}
	}
	
	public void lendStatus() {
		Connection conn = null;
		PreparedStatement ps = null;
		Integer bookLendId = getBookLendId();
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("UPDATE lend SET Returned=? WHERE BookLendId="+bookLendId+"");
			ps.setString(1, "Yes");
			ps.executeQuery();
			}
			catch(SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public static ArrayList<BookLend> getResultHistory(){
		String query = getSearchString();
		ArrayList<BookLend> bookHistory = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try{
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			ps = conn.prepareStatement("SELECT * From Lend Where bookId like '"+query+"'");
			rs = ps.executeQuery();
			while(rs.next()) {
				BookLend bookLend = new BookLend();
				bookLend.setBookLendId(rs.getInt("BookLendId"));
				bookLend.setBookId(rs.getInt("bookId"));
				bookLend.setBorrowerName(rs.getString("BorrowerName"));
				bookLend.setBookBorrowedDate(rs.getString("DateBorrowed"));
				bookLend.setBookDueDate(rs.getString("DateDue"));
				bookLend.setBookDateReturned(rs.getString("DateReturned"));
				bookHistory.add(bookLend);
				
				
			}
		}catch(SQLException sqle) {
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
				ps.close();
				rs.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		
		}
		return bookHistory;
	}
	
//	public ArrayList<BookLend> viewBookHistory(){
//		ArrayList<BookLend> bookHistory = new ArrayList<>();
//		String searchHistoryString = getSearchHistoryString();
////		String searchId = getSearchString();
//	
//		Connection conn = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		
//		
//		try {
//			Class.forName("org.mariadb.jdbc.Driver");
//		}catch(ClassNotFoundException cnfe) {
//			cnfe.printStackTrace();
//		}
//		if(searchHistoryString!=null) {
//		try {
//			conn = DriverManager.getConnection(url,dbuser,dbpass);
//			ps = conn.prepareStatement("SELECT * from lend where bookId="+searchHistoryString+"");
//			rs = ps.executeQuery();
//			while(rs.next()) {
//			BookLend bookLend = new BookLend();
//			bookLend.setBookLendId(rs.getInt("BookLendId"));
//			bookLend.setBookId(rs.getInt("bookId"));
//			bookLend.setBorrowerName(rs.getString("BorrowerName"));
//			bookLend.setBookBorrowedDate(rs.getString("DateBorrowed"));
//			bookLend.setBookDueDate(rs.getString("DateDue"));
//			bookLend.setBookDateReturned(rs.getString("DateReturned"));
//			bookHistory.add(bookLend);
//			}
//			
//		}catch(SQLException sqle) {
//			sqle.printStackTrace();
//		}finally {
//			try {
//				conn.close();
//				rs.close();
//				ps.close();
//			}catch(SQLException sqle) {
//				sqle.printStackTrace();
//			}
//		  }
//		}
//		return bookHistory;
//		
//	}
}
