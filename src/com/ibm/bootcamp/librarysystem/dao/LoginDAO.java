package com.ibm.bootcamp.librarysystem.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ibm.bootcamp.librarysystem.model.User;

public class LoginDAO {			
	private final String url = "jdbc:mariadb://localhost:3306/projectbootcamp";
	private final String dbuser = "root";
	private final String dbpass = "root";
	
	public String getUrl() {
		return url;
	}
	public String getDbuser() {
		return dbuser;
	}
	public String getDbpass() {
		return dbpass;
	}
	
	public User getUser(String userName, String userPassword) {
		
		Connection conn = null;
		ResultSet rs = null;
		User account = null;
		
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			conn = DriverManager.getConnection(url,dbuser,dbpass);
			PreparedStatement ps = conn.prepareStatement("SELECT * from user where Username=? and Password=?");
			ps.setString(1, userName);
			ps.setString(2, userPassword);
			rs = ps.executeQuery();
			while(rs.next()) {
				String username = rs.getString("Username");
				String password = rs.getString("Password");
				account = new User(username,password);
			}
			
			
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}finally {
			try {
				conn.close();
			}catch(SQLException sqle) {
				sqle.printStackTrace();
			}
		}
		
		return account;
		
	}
}
