package com.ibm.bootcamp.librarysystem.model;

public class BookLend {
	private Integer bookLendId, bookId;
	private String borrowerName, bookBorrowedDate, bookDueDate, bookDateReturned;	
	
	
	public BookLend() {
		
	}
	
	@Override
	public String toString() {
		return "BookLend [bookLendId=" + bookLendId + ", bookId=" + bookId + ", borrowerName=" + borrowerName
				+ ", bookBorrowedDate=" + bookBorrowedDate + ", bookDueDate=" + bookDueDate + ", bookDateReturned="
				+ bookDateReturned + "]";
	}

	public BookLend(Integer bookLendId, Integer bookId, String borrowerName, String bookBorrowedDate,
			String bookDueDate, String bookDateReturned) {
		super();
		this.bookLendId = bookLendId;
		this.bookId = bookId;
		this.borrowerName= borrowerName;;
		this.bookBorrowedDate = bookBorrowedDate;
		this.bookDueDate = bookDueDate;
		this.bookDateReturned = bookDateReturned;
	}

	public Integer getBookLendId() {
		return bookLendId;
	}

	public Integer getBookId() {
		return bookId;
	}

	public String getBookBorrowedDate() {
		return bookBorrowedDate;
	}

	public String getBookDueDate() {
		return bookDueDate;
	}

	public String getBookDateReturned() {
		return bookDateReturned;
	}

	public void setBookLendId(Integer bookLendId) {
		this.bookLendId = bookLendId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public void setBookBorrowedDate(String bookBorrowedDate) {
		this.bookBorrowedDate = bookBorrowedDate;
	}
	
	public void setBookDueDate(String bookDueDate) {
		this.bookDueDate = bookDueDate;
	}

	public void setBookDateReturned(String bookDateReturned) {
		this.bookDateReturned = bookDateReturned;
	}

	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}
	
}
