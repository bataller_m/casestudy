package com.ibm.bootcamp.librarysystem.model;

public class Book {
	private Integer bookId;
	private String bookTitle, bookAuthor, bookIsbn, bookSubject, bookLocation, bookDatePublished,
	bookDateAcquired;
	private String bookStatus;
	
	public Book() {
		
	}
		
	public Book (Integer book_ID, String book_Title, String book_Author, String book_ISBN, String book_Subject,
			String book_Location, String book_Date_Published, String book_Date_Acquired, String book_Status) {
		super();
		this.bookId = book_ID;
		this.bookTitle = book_Title;
		this.bookAuthor = book_Author;
		this.bookIsbn = book_ISBN;
		this.bookSubject = book_Subject;
		this.bookLocation = book_Location;
		this.bookDatePublished = book_Date_Published;
		this.bookDateAcquired = book_Date_Acquired;
		this.bookStatus = book_Status;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookTitle=" + bookTitle + ", bookAuthor=" + bookAuthor
				+ ", bookIsbn=" + bookIsbn + ", bookSubject=" + bookSubject + ", bookLocation=" + bookLocation
				+ ", bookDatePublished=" + bookDatePublished + ", bookDateAcquired=" + bookDateAcquired
				+ ", bookStatus=" + bookStatus + "]";
	}
	public Integer getBookId() {
		return bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public String getBookAuthor() {
		return bookAuthor;
	}
	public String getBookIsbn() {
		return bookIsbn;
	}
	public String getBookSubject() {
		return bookSubject;
	}
	public String getBookLocation() {
		return bookLocation;
	}
	public String getBookDatePublished() {
		return bookDatePublished;
	}
	public String getBookDateAcquired() {
		return bookDateAcquired;
	}
	public String getBookStatus() {
		return bookStatus;
	}
	public void setBookId(Integer book_ID) {
		this.bookId = book_ID;
	}
	public void setBookTitle(String book_Title) {
		this.bookTitle = book_Title;
	}
	public void setBookAuthor(String book_Author) {
		this.bookAuthor = book_Author;
	}
	public void setBookIsbn(String book_ISBN) {
		this.bookIsbn = book_ISBN;
	}
	public void setBookSubject(String book_Subject) {
		this.bookSubject = book_Subject;
	}
	public void setBookLocation(String book_Location) {
		this.bookLocation = book_Location;
	}
	public void setBookDatePublished(String book_Date_Published) {
		this.bookDatePublished = book_Date_Published;
	}
	public void setBookDateAcquired(String book_Date_Acquired) {
		this.bookDateAcquired = book_Date_Acquired;
	}
	public void setBookStatus(String book_Status) {
		this.bookStatus = book_Status;
	}
	
	
	
}
