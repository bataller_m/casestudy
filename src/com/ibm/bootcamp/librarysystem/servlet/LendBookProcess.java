package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet implementation class LendBookProcess
 */
@WebServlet("/LendBookProcess/*")
public class LendBookProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LendBookProcess() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Calendar currentDate = Calendar.getInstance();
		java.sql.Date date = new java.sql.Date(currentDate.getTimeInMillis());
		HttpSession session = request.getSession();
		
		session.setAttribute("DateFormat", date);
		//
		//Send Details only of Book ID.
		Integer lendBookId = Integer.parseInt(request.getParameter("id"));
		//
		session.setAttribute("lendBookId", lendBookId);
		RequestDispatcher rd = request.getRequestDispatcher("LendBookConfirm.jsp");
		rd.forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Create Calendar Instance
		
	}

}
