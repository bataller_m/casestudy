package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.PrintWriter;
import java.sql.SQLIntegrityConstraintViolationException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ibm.bootcamp.librarysystem.dao.BookDAO;
import com.ibm.bootcamp.librarysystem.model.BookLend;

/**
 * Servlet implementation class ConfirmLend
 */
@WebServlet("/ConfirmLend")
public class ConfirmLend extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmLend() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NotSerializableException{
		
		BookDAO bookDao = new BookDAO();
		Integer bookLendId = 0;
		Integer bookId = Integer.parseInt(request.getParameter("lendBookId"));
		String borrowerName = request.getParameter("lendBookBorrowerName");
		String borrowedDate = request.getParameter("lendBookBorrowedDate");
		String dueDate = request.getParameter("lendBookDueDate");
		String dateReturned= null;
		BookLend bookLend = new BookLend(bookLendId,bookId,borrowerName,borrowedDate,dueDate,dateReturned);
		
		String condition = request.getParameter("button");
	
		try {
		if(condition.equals("ConfirmLend")) {
			BookDAO.setId(bookId);
			bookDao.lendBook(bookLend);
			bookDao.lendBookProxy(bookLend);
			bookDao.updateStatus();
			PrintWriter out = response.getWriter();
			out.println("<script type='text/javascript'>");
		    out.println("alert('Your book lending was registered.');");
		    out.println("location='Homepage.jsp';");
		    out.println("</script>");

		}
		if(condition.equals("cancel")) {
			response.sendRedirect("LendBook.jsp");
		}
		}catch(NullPointerException npe) {
			npe.printStackTrace();
		}catch(SQLIntegrityConstraintViolationException e) {
			e.printStackTrace(); 
		}catch(NotSerializableException nse) {
			nse.printStackTrace();
		}
//		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
