package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.bootcamp.librarysystem.dao.BookDAO;
import com.ibm.bootcamp.librarysystem.model.Book;

/**
 * Servlet implementation class UpdateBook
 */
@WebServlet("/UpdateBook")
public class UpdateBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookDAO bookDao = new BookDAO();
		Integer bookID = Integer.parseInt(request.getParameter("bookID"));
		
		BookDAO.setId(bookID);
		String bookTitle = request.getParameter("bookTitle");
		String bookAuthor = request.getParameter("bookAuthor");
		String bookIsbn = request.getParameter("bookIsbn");
		String bookSubject = request.getParameter("bookSubject");
		String bookLocation = request.getParameter("bookLocation");
		String bookDatePublished = request.getParameter("bookDatePublished");
		String bookDateAcquired = request.getParameter("bookDateAcquired");
		String bookStatus = request.getParameter("bookStatus");
		Book book = new Book(bookID,bookTitle,bookAuthor,bookIsbn,bookSubject,bookLocation,bookDatePublished,bookDateAcquired,bookStatus);
		
		
		String condition = request.getParameter("button");
		System.out.println(condition);
		try {
		if(condition.equals("Update")) {
			PrintWriter out = response.getWriter();
			out.println("<script type='text/javascript'>");
		    out.println("alert('Book Updated Sucessfully');");
		    out.println("location='ViewBooks.jsp';");
		    out.println("</script>");
			bookDao.updateBook(book);
//			response.sendRedirect("ViewBooks.jsp");
			
			
		}
		if(condition.equals("Delete")) {
			bookDao.deleteBook();
			response.sendRedirect("ViewBooks.jsp");
		}
		if(condition.equals("CancelEdit")) {
			response.sendRedirect("ViewBooks.jsp");
		}
		
		}catch(NullPointerException npe) {
			npe.printStackTrace();
		}
		
	}

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
