package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.bootcamp.librarysystem.dao.LoginDAO;
import com.ibm.bootcamp.librarysystem.model.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginDAO loginDao = new LoginDAO();
		User user = loginDao.getUser(request.getParameter("username"), request.getParameter("password"));
		PrintWriter out = response.getWriter();
		if(user!=null) {
			out.println("<script type='text/javascript'>");
		    out.println("alert('Logged in Sucessfully');");
		    out.println("location='Homepage.jsp';");
		    out.println("</script>");
		     
//		    response.sendRedirect("index.jsp");
		    
		}else {
			out.println("<script type='text/javascript'>");
		    out.println("alert('Wrong Username or Password');");
		    out.println("location='Login.jsp';");
		    out.println("</script>");
//			response.sendRedirect("default.jsp");
		}
		
	
	}

}
