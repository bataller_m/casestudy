package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.bootcamp.librarysystem.dao.BookDAO;

/**
 * Servlet implementation class ConfirmReturn
 */
@WebServlet("/ConfirmReturn")
public class ConfirmReturn extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfirmReturn() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer returnBookId = Integer.parseInt(request.getParameter("returnBookId"));
		
		Calendar currentDate = Calendar.getInstance();
		
		java.sql.Date date = new java.sql.Date(currentDate.getTimeInMillis());
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = df.format(date);
	
		BookDAO bookDao = new BookDAO();
		
		
		BookDAO.setDateReturned(dateString);
		BookDAO.setBookLendId(returnBookId);
		
		
		Integer e = bookDao.returnBookID();
		BookDAO.setId(e);
		bookDao.updateStatusReturn();
		bookDao.updateReturn(); /* This update the return date*/
		bookDao.lendStatus();
		response.sendRedirect("ReturnBook");
		PrintWriter out = response.getWriter();
		out.println("<script type='text/javascript'>");
	    out.println("alert('The book you lend was returned');");
	    out.println("location='Homepage.jsp';");
	    out.println("</script>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
