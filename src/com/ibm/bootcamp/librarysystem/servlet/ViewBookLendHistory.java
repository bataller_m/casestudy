package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ibm.bootcamp.librarysystem.dao.BookDAO;
import com.ibm.bootcamp.librarysystem.model.BookLend;

/**
 * Servlet implementation class ViewBookLendHistory
 */
@WebServlet("/ViewBookLendHistory")
public class ViewBookLendHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewBookLendHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		String searchStringBookId = request.getParameter("viewBookHistory");
		if(searchStringBookId==null) {
			System.out.println("This is null");
		}
		BookDAO.setSearchString(searchStringBookId);
//		Integer searchBookId = Integer.parseInt(searchStringBookId);
//		BookDAO bookDao = new BookDAO();
		
//		BookDAO.setId(searchBookId);
		System.out.println(searchStringBookId);
//		ArrayList<BookLend> bookLendHistory = bookDao.viewBookHistory();
		ArrayList<BookLend> bookLendHistory = BookDAO.getResultHistory();
		HttpSession session = request.getSession();
		session.setAttribute("bookLendHistory", bookLendHistory);
		
		RequestDispatcher rd = request.getRequestDispatcher("ViewBookHistory.jsp");
		rd.forward(request, response);
//		}catch(NumberFormatException nfe) {
//			nfe.printStackTrace();
//		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
