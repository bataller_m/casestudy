package com.ibm.bootcamp.librarysystem.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.bootcamp.librarysystem.dao.BookDAO;
import com.ibm.bootcamp.librarysystem.model.Book;


/**
 * Servlet implementation class AddBook
 */
@WebServlet("/AddBook")
public class AddBook extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddBook() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	BookDAO bookDao = new BookDAO();
	Integer book_ID = 0;
	String book_Title = request.getParameter("bookTitle");
	String book_Author = request.getParameter("bookAuthor");
	String book_ISBN = request.getParameter("bookISBN");
	String book_Subject = request.getParameter("bookSubject");
	String book_Location = request.getParameter("bookLocation");
	String book_Date_Published = request.getParameter("bookDatePublished");
	String book_Date_Acquired = request.getParameter("bookDateAcquired");
	String book_Status = "Yes";
	
	Book book = new Book(book_ID, book_Title, book_Author, book_ISBN, book_Subject, book_Location, book_Date_Published,
	book_Date_Acquired, book_Status);
	
	if(book!=null) {
	PrintWriter out = response.getWriter();
	out.println("<script type='text/javascript'>");
	out.println("alert('Successfully Added Book');");
	out.println("location='Homepage.jsp';");
	out.println("</script>");
	bookDao.addBook(book);
//	response.sendRedirect("AddBook.jsp");
	}
  }
}
