<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add a Book</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li  class="active"><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>
<form method="POST" action="AddBook">
<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
            <tr>
               <th>Add a new book.</th>
               <th></th>
            </tr>
        </thead>
			<tr>
                <td>Book ID</td>
                <td>Book ID will be generated automatically.</td>
            </tr>
            <tr>
                <td>Title:</td>
                <td><input type="text" required placeholder="Enter Title..." name="bookTitle" id="booktitle" class="bookTitle"></input></td>
            </tr>
            <tr>
                <td>Author/s:</td>
            	<td><input type="text" required placeholder="Enter Author/s..." name="bookAuthor" id="bookAuthor" class="bookAuthor"></input></td>
            </tr>
            <tr>    
                <td>ISBN:</td>
                <td><input type="text" required placeholder="Enter ISBN..." name="bookISBN" id="bookISBN" class="bookISBN" maxlength="13" size="13"></input></td>
            </tr>
            <tr>
                <td>Subject:</td>
                <td><input type="text" required placeholder="Enter Subject..." name="bookSubject" id="bookSubject" class="bookSubject"></input></td>
            </tr>
            <tr>    
                <td>Location: </td>
                <td><input type="text" required placeholder="Enter location..." name="bookLocation" id="bookLocation" class="bookLocation"></input></td>
            </tr>    
            <tr>
                <td>Date Published: </td>
                <td><input type="date" required name="bookDatePublished" id="bookDatePublished" class="bookDatePublished"></input></td>
            </tr>
            <tr>    
                <td>Date Acquired:</td>
                <td><input type="date" required name="bookDateAcquired" id="bookDateAcquired" class="bookDateAcquired"></input></td>
            </tr>
            <tr>
            <td align="center"><button type="submit" name="AddBook">Submit</button></td>
            <td align="center"><button type="reset" name="CancelAdd">Cancel</button></td>
            </tr>
</table>
</form>
</body>
</html>