<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.ArrayList" %>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li class="active"><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
      </ul>
  </div>
  </nav>

  <table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col">Book ID</th>
      <th style="text-align:center" scope="col">Title</th>
      <th style="text-align:center" scope="col">Author(s)</th>
      <th style="text-align:center" scope="col">ISBN</th>
      <th style="text-align:center" scope="col">Subject</th>
      <th style="text-align:center" scope="col">Location</th>
      <th style="text-align:center" scope="col">Date Published</th>
      <th style="text-align:center" scope="col">Date Acquired</th>
      <th style="text-align:center" scope="col">Action</th>
  </tr>
  <c:forEach items="${lendBook}" var="lendBook">
   <tr>
  <td align="center">${lendBook.getBookId()}</td>
  <td align="center">${lendBook.getBookTitle()}</td>
  <td align="center">${lendBook.getBookAuthor()}</td>
  <td align="center">${lendBook.getBookIsbn()}</td>
  <td align="center">${lendBook.getBookSubject()}</td>
  <td align="center">${lendBook.getBookLocation()}</td>
  <td align="center">${lendBook.getBookDatePublished()}</td>
  <td align="center">${lendBook.getBookDateAcquired()}</td>
  <td align="center"><a href="LendBookProcess?id=${lendBook.getBookId()}">Lend</a></td>
  </tr>
  </c:forEach>
  </table>

</body>
</html>