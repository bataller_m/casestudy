<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find a Book</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li class="active"><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>
<form action="SearchBook">
<table class="table table-striped table-bordered" style="width:25%">
<tr>
<td><input type="text" name="searchBox" placeholder="Input fields here..."></td>
</tr>
<tr>
<td><button type="submit">Find Book</button></td>
</tr>
</table>
</form>

<table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col" >Book ID</th>
      <th style="text-align:center" scope="col" >Title</th>
      <th style="text-align:center" scope="col" >Author(s)</th>
      <th style="text-align:center" scope="col" >ISBN</th>
      <th style="text-align:center" scope="col" >Subject</th>
      <th style="text-align:center" scope="col" >Location</th>
      <th style="text-align:center" scope="col" >Date Published</th>
      <th style="text-align:center" scope="col" >Date Acquired</th>
      <th style="text-align:center" scope="col" >Available?</th>
  </tr>
  <c:forEach items="${bookSearch}" var="myBook">

  <tr>
  <td align="center">${myBook.getBookId()}</td>
  <td align="center">${myBook.getBookTitle()}</td>
  <td align="center">${myBook.getBookAuthor()}</td>
  <td align="center">${myBook.getBookIsbn()}</td>
  <td align="center">${myBook.getBookSubject()}</td>
  <td align="center">${myBook.getBookLocation()}</td>
  <td align="center">${myBook.getBookDatePublished()}</td>
  <td align="center">${myBook.getBookDateAcquired()}</td>
  <td align="center">${myBook.getBookStatus()}</td>
  </tr>

  </c:forEach>
  </table>
  
</body>
</html>