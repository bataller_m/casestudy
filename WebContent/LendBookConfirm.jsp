<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lend a Book</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li ><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>
<form action="ConfirmLend">
<table border="1" class="table">
  <tr>
      <th scope="col"></th>
      <th></th>
      <tr>
      <td>Book Lend ID</td>
      <td>Book Lend ID will be generated automatically.</td>
  </tr>
   <tr>
  <td><label>Book ID:</label></td>
  <td align="left"><input value="${lendBookId}" name="lendBookId" readonly></td>
  </tr>
   <tr>
  <td><label>Borrower's Name</label></td>
  <td align="left"><input type="text" name="lendBookBorrowerName" placeholder="Enter borrower's name here ..."  width="100px"></td>
  </tr>
   <tr>
  <td><label>Date Borrowed: </label></td>
  <td align="left"><input type="date" name="lendBookBorrowedDate" value="${DateFormat}" readonly></td>
  </tr>
  <tr>
  <td><label>Date Due: </label></td>
  <td align="left"><input  type="date" name="lendBookDueDate"></td>
  </tr>
  
  <tr>
  <td align="center"><button type="submit" name="button" value="ConfirmLend">Confirm</button></td>
  <td align="center"><button type="submit"  name="button" value="cancel">Cancel</button></td>
  <td></td>
  </tr>
  </table>
</form>

</body>
</html>