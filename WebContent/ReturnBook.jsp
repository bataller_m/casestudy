<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li  class="active"><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
      </ul>
  </div>
</nav>
 <table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col">Lend Book</th>
      <th style="text-align:center" scope="col">Book ID</th>
      <th style="text-align:center" scope="col">Borrower's Name</th>
      <th style="text-align:center" scope="col">Date Borrowed</th>
      <th style="text-align:center" scope="col">Date Due</th>
      <th style="text-align:center" scope="col">Action</th>
  </tr>
  <c:forEach items="${returnList}" var="returnList">
   <tr>
  <td align="center">${returnList.getBookLendId()}</td>
  <td align="center"><input value="${returnList.getBookId()}" name="borrowBookId" hidden>${returnList.getBookId()}</td>
  <td align="center">${returnList.getBorrowerName()}</td>
  <td align="center">${returnList.getBookBorrowedDate()}</td>
  <td align="center">${returnList.getBookDueDate()}</td>
  <td align="center"><a href="ReturnBookProcess?returnid=${returnList.getBookLendId()}">Return</a></td>
  </tr>
  </c:forEach>
  </table>

</body>
</html>