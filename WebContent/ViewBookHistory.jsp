<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@page import="com.ibm.bootcamp.librarysystem.model.BookLend" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li class="active"><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>

<form action="ViewBookLendHistory">
<table class="table table-striped table-bordered" style="width:25%">
<tr>
<th scope="col">Edit a book.</th>
</tr>
<tr>
<td><input type="text" name="viewBookHistory" placeholder="Enter Book Id"></td>
</tr>
<tr>
<td><button type="Submit">Edit Book</button></td>
</tr>
</table>
</form>

 <table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col">Book Lend ID</th>
      <th style="text-align:center" scope="col">Book ID</th>
      <th style="text-align:center" scope="col">Borrower's Name</th>
      <th style="text-align:center" scope="col">Book Borrowed Date</th>
      <th style="text-align:center" scope="col">Book Due Date</th>
      <th style="text-align:center" scope="col">Book Date Returned</th>

  </tr>
  <c:forEach items="${bookLendHistory}" var="myBook">
   <tr>
  <td align="center">${myBook.getBookLendId()}</td>
  <td align="center">${myBook.getBookId()}</td>
  <td align="center">${myBook.getBorrowerName()}</td>
  <td align="center">${myBook.getBookBorrowedDate()}</td>
  <td align="center">${myBook.getBookDueDate()}</td>
  <td align="center">${myBook.getBookDateReturned()}</td>
  </tr>
  </c:forEach>
  </table>
</body>
</html>