<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    
  </div>
</nav>

<form method="POST" action="Login">
<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
            <tr>
               <th>Please log-in</th>
               <th></th>
            </tr>
        </thead>
			<tr>
			<td>User name</td>
			<td><input type="text" placeholder="Enter username..." class="username" name="username" id="username"></input></td>
			</tr>
			<tr>
			<td>Password</td>
			<td><input type="password" placeholder="Enter password..." class="password" name="password" id="password"></input></td>
			</tr>
			<tr><td><button type="submit">Log-in</button></td>
			<td><button type="reset">Reset</button>
			</tr>	
</table>
</form>

</body>
</html>