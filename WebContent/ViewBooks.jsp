<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList" %>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Books</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li class="active"><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>

<form action="EditBook">
<table class="table table-striped table-bordered" style="width:25%">
<tr>
<th scope="col">Edit a book.</th>
</tr>
<tr>
<td><input type="text" name="editBox" placeholder="Enter Book ID here to edit book." maxlength="20" size="20"></td>
</tr>
<tr>
<td><button type="Submit">Edit Book</button></td>
</tr>
</table>
</form>

 <table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col">Book ID</th>
      <th style="text-align:center" scope="col">Title</th>
      <th style="text-align:center" scope="col">Author(s)</th>
      <th style="text-align:center" scope="col">ISBN</th>
      <th style="text-align:center" scope="col">Subject</th>
      <th style="text-align:center" scope="col">Location</th>
      <th style="text-align:center" scope="col">Date Published</th>
      <th style="text-align:center" scope="col">Date Acquired</th>
  </tr>
  <c:forEach items="${bookList}" var="myBook">
   <tr>
  <td align="center">${myBook.getBookId()}</td>
  <td align="center">${myBook.getBookTitle()}</td>
  <td align="center">${myBook.getBookAuthor()}</td>
  <td align="center">${myBook.getBookIsbn()}</td>
  <td align="center">${myBook.getBookSubject()}</td>
  <td align="center">${myBook.getBookLocation()}</td>
  <td align="center">${myBook.getBookDatePublished()}</td>
  <td align="center">${myBook.getBookDateAcquired()}</td>
  </tr>
  </c:forEach>
  </table>
</body>
</html>