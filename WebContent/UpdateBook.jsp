<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.ibm.bootcamp.librarysystem.dao.BookDAO" %>
<%@page import="com.ibm.bootcamp.librarysystem.model.Book" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li><a href="ReturnBook">Return a Book</a></li>
      <li class="active"><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
    </ul>
  </div>
</nav>
<form action="UpdateBook">
<table border="1" class="table">
  <tr>
      <th scope="col">Details:</th>
      <th>You can edit the book's location and its status by typing 'Yes' or 'No'.</th>
      
  </tr>
  <c:forEach items="${bookEdit}" var="myBook">
   <tr>
  <td><label>Book ID:</label></td>
  <td align="left"><input value="${myBook.getBookId()}" name="bookID" readonly></td>
  </tr>
  <tr>
  <td><label>Title: </label></td>
  <td align="left"><input value="${myBook.getBookTitle()}" name="bookTitle"readonly></td>
  </tr>
   <tr>
  <td><label>Author/s: </label></td>
  <td align="left"><input value="${myBook.getBookAuthor()}" name="bookAuthor" readonly></td>
  </tr>
   <tr>
  <td><label>ISBN: </label></td>
  <td align="left"><input value="${myBook.getBookIsbn()}" name="bookISBN" readonly></td>
  </tr>
  <tr>
  <td><label>Subject: </label></td>
  <td align="left"><input value="${myBook.getBookSubject()}" name="bookSubject" readonly></td>
  </tr>
   <tr>
  <td><label>Location: </label></td>
  <td align="left"><input type="text" value="${myBook.getBookLocation()}" name="bookLocation"></td>
  </tr>
   <tr>
  <td><label>Date Published: </label></td>
  <td align="left"><input type="date" value="${myBook.getBookDatePublished()}" name="bookDatePublished" readonly></td>
  </tr>
  <tr>
  <td><label>Date Acquired: </label></td>
  <td align="left"><input type="date" value="${myBook.getBookDateAcquired()}" name="bookDateAcquired" readonly></td>
  </tr>
  <tr>
  <td><label>Status: </label></td>
  <td align="left"><input type="text" value="${myBook.getBookStatus()}" name="bookStatus"></td>
  </tr>
  <tr>
  <tr>
  <td align="center"><button type="submit" value="Update" name="button" value="Update">Update</button></td>
  <td align="center"><button type="submit" value="Delete" name="button" value="Delete">Delete</button></td>
  <td align="center"><button type="submit" value="Delete" name="button" value="CancelEdit">Cancel</button></td>
  </tr>
  </c:forEach>
  </table>
</form>

</body>
</html>