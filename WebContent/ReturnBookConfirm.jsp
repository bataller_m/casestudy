<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" >MyLibrarySystem</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Homepage.jsp">Home</a></li>
      <li><a href="AddBook.jsp">Add a Book</a></li>
      <li><a href="BookSearch.jsp">Search for a Book</a></li>
      <li><a href="LendBook">Lend a Book</a></li>
      <li  class="active"><a href="ReturnBook">Return a Book</a></li>
      <li><a href="ViewBook">View Books</a></li>
      <li><a href="ViewBookHistory.jsp">Book Lending History</a></li>
      <li><a href="Login.jsp">Login</a></li>
      </ul>
  </div>  
</nav>
<div>
<form action="ConfirmReturn">
<table border="1" class="table">
  <tr>
      <th style="text-align:center" scope="col" width="50px;">Return Book?</th>
  </tr>
   <tr>
  <td align="center"><input value="${returnBookId}" name="returnBookId" hidden>Book Lend ID: ${returnBookId}</td>
  </tr>
  <tr align="center" >
  <td><button type="submit" name="button" value="confirmReturn">Confirm</button>&nbsp;&nbsp;&nbsp;<button type="submit"  name="button" value="cancelReturn">Cancel</button></td>
  </tr>
</table>
</form>
</div>
</body>
</html>